# Bitbucket Issues convenience #

A Firefox add-on to improve Bitbucket Issues UI.

Unofficial add-on. Not affiliated with Bitbucket or Atlassian.

## Status ##

Currently supports toggling column visibility.

## Stylish ##

[Stylish](https://addons.mozilla.org/en-US/firefox/addon/stylish/) users, try this user style instead: https://userstyles.org/styles/131061/bitbucket-issues-column-visibility so you can avoid installing another extension.

## Contributing ##

Please check out the [issues page](https://bitbucket.org/talklittle/bitbucket-issues-firefox/issues).

Info on add-on development: https://developer.mozilla.org/en-US/Add-ons/SDK

https://developer.mozilla.org/en-US/Add-ons/SDK/High-Level_APIs/page-mod