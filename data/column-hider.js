var showColumnPrefToClass = self.options.showColumnPrefToClass;

self.port.on("showHideColumns", function(prefs) {
    var prefKeys = Object.keys(showColumnPrefToClass);
    for (var i = 0; i < prefKeys.length; i++) {
        var prefKey = prefKeys[i];
        if (prefs[prefKey] == null) {
            continue;
        }

        var showColumn = !!prefs[prefKey];
        var className = showColumnPrefToClass[prefKey];

        var cells = document.querySelectorAll('td.' + className + ', th.' + className);
        for (var cellIndex = 0; cellIndex < cells.length; cellIndex++) {
            cells[cellIndex].style.display = showColumn ? 'table-cell' : 'none';
        }
    }
});