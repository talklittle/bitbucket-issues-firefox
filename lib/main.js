var data = require("sdk/self").data;
var pageMod = require("sdk/page-mod");
var simplePrefs = require("sdk/simple-prefs");

var mappings = require("./mappings.js");

function getInitialContentStyle() {
    var prefs = simplePrefs.prefs;
    var showColumnPrefToClass = mappings.getShowColumnPrefToClassMap();

    var result = [];

    var prefKeys = Object.keys(showColumnPrefToClass);
    for (var i = 0; i < prefKeys.length; i++) {
        var prefKey = prefKeys[i];
        if (prefs[prefKey] == null) {
            continue;
        }

        var showColumn = !!prefs[prefKey];
        var className = showColumnPrefToClass[prefKey];

        var cssDisplayValue = (showColumn ? 'table-cell' : 'none');
        result.push("td." + className + " { display: " + cssDisplayValue + " }");
        result.push("th." + className + " { display: " + cssDisplayValue + " }");
    }
    return result;
}

pageMod.PageMod({
    include: /https:\/\/bitbucket\.org\/[^\/]*\/[^\/]*\/issues($|[?\/].*)/,

    contentScriptFile: data.url("column-hider.js"),
    contentScriptOptions: {
        showColumnPrefToClass: mappings.getShowColumnPrefToClassMap()
    },

    contentStyle: getInitialContentStyle(),

    onAttach: function(worker) {
        worker.port.emit("showHideColumns", simplePrefs.prefs);

        var onPrefChange = function(prefName) {
            var changed = {};
            changed[prefName] = simplePrefs.prefs[prefName];
            worker.port.emit("showHideColumns", changed);
        };
        require("sdk/simple-prefs").on("", onPrefChange);
        worker.on('detach', function () {
            simplePrefs.removeListener("", onPrefChange);
        });
    }
});
