// CSS class => Header name
function getClassToHeaderMap() {
    return {
        text: 'Title',
        'icon-col': 'Type',  // TODO icon-col is both Type and Priority
        state: 'Status',
        votes: 'Votes',
        user: 'Assignee',
        milestone: 'Milestone',
        version: 'Version',
        date: 'Created'  // TODO date is both Created and Updated
    };
}

// Pref key => CSS class
function getShowColumnPrefToClassMap() {
    return {
        showColumnTitle: 'text',
        showColumnTypeAndPriority: 'icon-col',
        showColumnStatus: 'state',
        showColumnVotes: 'votes',
        showColumnAssignee: 'user',
        showColumnMilestone: 'milestone',
        showColumnVersion: 'version',
        showColumnCreatedAndUpdated: 'date'
    };
}

exports.getClassToHeaderMap = getClassToHeaderMap;
exports.getShowColumnPrefToClassMap = getShowColumnPrefToClassMap;
